<?php

namespace App;

class Craps
{
    /** @var Dice */
    protected $dice;

    /** @var string|null */
    private $result = null;

    /** @var array */
    private $rolls = [];

    public function __construct(\App\Dice $dice)
    {
        $this->dice = $dice;
    }

    public function playRound()
    {
        $firstThrow = $this->dice->roll();
        $this->rolls[] = $firstThrow;

        if($firstThrow == 7 || $firstThrow == 11) {
            $this->result = 'win';
            return;
        }

        if ($firstThrow == 2 || $firstThrow == 3 || $firstThrow == 12) {
            $this->result = 'lose';
            return;
        }

        $throw = $this->dice->roll();
        $this->rolls[] = $throw;
        while($throw != $firstThrow && $throw != 7) {
            $throw = $this->dice->roll();
            $this->rolls[] = $throw;
        }

        if ($throw == $firstThrow) {
            $this->result = 'win';
        } else {
            $this->result = 'lose';
        }
    }

    public function result()
    {
        return $this->result;
    }

    public function output(): array
    {
        return $this->rolls;
    }
}
