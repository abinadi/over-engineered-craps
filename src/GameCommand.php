<?php

declare(strict_types=1);

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class GameCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'play:craps';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Lets you play a game of craps.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to play some craps but not for money.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write(sprintf("\033\143"));
        $output->writeln("====================================");
        $output->writeln("== Let's play some craps!         ==");
        $output->writeln("====================================");

        $keepPlaying = true;

        while ($keepPlaying) {
            $game = new Craps(new Dice(new SingleDie(), new SingleDie()));
            $game->playRound();
            $output->writeln('');
            foreach($game->output() as $roll) {
                $output->writeln('You rolled a: ' . $roll);
            }
            if ($game->result() == 'win') {
                $output->writeln('You won!');
            } else {
                $output->writeln('You lost. :( How sad for you.');
            }

            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('Do you want to keep playing? [y/N]', false);
            $keepPlaying = $helper->ask($input, $output, $question);
        }
    }
}
