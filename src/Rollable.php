<?php

namespace App;

interface Rollable
{
    public function roll(): int;
}