<?php

declare(strict_types=1);

namespace Tests;

use App\Craps;
use App\Dice;
use App\SingleDie;
use PHPUnit\Framework\TestCase;

class CrapsTest extends TestCase
{
    /**
     * @test
     * @dataProvider winningFirstRolls
     * @param FakeDie $die1
     * @param FakeDie $die2
     */
    public function will_win_on_rolling_a_seven_or_eleven_on_first_throw(FakeDie $die1, FakeDie $die2)
    {
        $craps = new \App\Craps(new Dice($die1, $die2));
        $craps->playRound();

        $this->assertEquals('win', $craps->result());
    }

    public function winningFirstRolls()
    {
        return [
            [new FakeDie(3), new FakeDie(4)],
            [new FakeDie(5), new FakeDie(6)]
        ];
    }

    /**
     * @param FakeDie $die1
     * @param FakeDie $die2
     * @test
     * @dataProvider losingFirstThrow
     */
    public function will_lost_on_rolling_a_two_three_or_twelve_on_first_throw(FakeDie $die1, FakeDie $die2)
    {
        $craps = new Craps(new Dice($die1, $die2));
        $craps->playRound();
        $this->assertEquals('lose', $craps->result());
    }

    public function losingFirstThrow()
    {
        return [
            [new FakeDie(1), new FakeDie(1)],
            [new FakeDie(2), new FakeDie(1)],
            [new FakeDie(6), new FakeDie(6)],
        ];
    }

    /** @test */
    public function will_keep_rolling_until_a_seven_or_point_is_thrown()
    {
        $craps = new Craps(new Dice(new FakeDieIncrementor(5), new FakeDie(3)));
        $craps->playRound();
        $this->assertEquals('lose', $craps->result());
        $this->assertCount(6, $craps->output());

        $craps = new Craps(new Dice(new FakeDie(2), new FakeDie(2)));
        $craps->playRound();
        $this->assertEquals('win', $craps->result());
        $this->assertCount(2, $craps->output());
    }
}
