<?php

namespace Tests;

use App\Rollable;

class FakeDieIncrementor implements Rollable
{
    /** @var int */
    private $toReturn;

    private $firstRoll = true;

    public function __construct(int $int)
    {
        $this->toReturn = $int;
    }

    public function roll(): int
    {
        if (! $this->firstRoll) {
            $this->toReturn = ($this->toReturn == 6) ? 1 : $this->toReturn + 1;
        }
        $this->firstRoll = false;
        return $this->toReturn;
    }
}
