<?php

declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\SingleDie;

class SingleDieTest extends TestCase
{
    /** @test */
    public function can_roll_a_random_digit_between_one_and_six()
    {
        $die = new SingleDie;

        $result = $die->roll();

        $this->assertIsNumeric($result);
        $this->assertLessThanOrEqual(6, $result);
        $this->assertGreaterThanOrEqual(1, $result);
    }
}
