<?php

declare(strict_types=1);

namespace Tests;

use App\Dice;
use App\SingleDie;
use PHPUnit\Framework\TestCase;

class DiceTest extends TestCase
{
    /** @test */
    public function can_roll_two_dice()
    {
        $dice = new Dice(new SingleDie(), new SingleDie());

        $result = $dice->roll();

        $this->assertIsNumeric($result);
        $this->assertLessThanOrEqual(12, $result);
        $this->assertGreaterThanOrEqual(2, $result);
    }
}
